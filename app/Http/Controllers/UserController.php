<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function show(Request $request){
        // $user = User::where('username', $username)->first();
        // $role = $user->merge($user->role);
        return User::whereFirst('username', $request->name)->with('role');
    }

    public function showAll(Request $request){
        // $user = User::where('username', $username)->first();
        // $role = $user->merge($user->role);
        return User::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($role_id, $username, $pasword)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'role_id' => 'required',
            'username' => 'required|unique:users',
            'pasword' => 'required'
        ]);

        User::create($validateData);
        return response()->json([
            'succes' => 'Sukses add data'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUserRequest  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // return $user;

        $validator = Validator::make($request->all(),[
            'username' =>  ['required',Rule::unique('users')->ignore(auth()->user()->id)],
            'pasword' => 'required|string|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $user = User::find(auth()->user()->id);
        $user->username = $request->username;
        $user->pasword = bcrypt($request->pasword);

        // $user->forceFill([
        //     'username' => $request->username,
        // ])->save();

        // $user->update([
        //     'username' => $request->username,
        // ]);

        if ($user->save()) {
            return response()->json([
                'message' => 'Sukses update data'
            ],200);
        }

        return response()->json([
            'message' => 'Gagal update data'
        ],401);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        // return $user;

        if(User::destroy($user->id)) return response()->json(['message' => 'Sukses delete data']);

        return response()->json(['message' => 'Failed delete data']);
    }

    public function authenticate(Request $request){
        $credential = $request->validate([
            'username' => 'required|string|max:255',
            'pasword' => 'required|string'
        ]);

        $auth = User::where('username', $request->username)->first();
        if  (!empty($auth)){

            if (password_verify($request->pasword,$auth->pasword)) {
                $token = $auth->createToken('auth_token')->plainTextToken;
                return response()->json([
                    'message' => 'Login Success',
                    'token' => $token,
                    'data' => $auth
                ],200);
            }
            return response()->json([
                'message' => 'Username atau password tidak cocok'
            ],401);

        }

        return response()->json([
            'message' => 'Username atau password tidak cocok'
        ],401);
    }

    public function logout(Request $request){
        try {
            $request->user()->tokens()->delete();

            return response()->json([
                'message' => 'Sukses logout'
            ]);
        } catch (\Exception $e){
            return response()->json(['message' => 'Login expired'], 401); // 401 pesan eror
        }
    }
}
