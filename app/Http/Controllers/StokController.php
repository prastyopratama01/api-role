<?php

namespace App\Http\Controllers;

use App\Models\Stok;
use App\Http\Requests\UpdateStokRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StokController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Stok::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // $validate = $request->validate([
        //     'nama' => 'required',
        //     'harga' => 'required',
        //     'image' => 'image|file|max:5000',
        //     'total_stok' => 'required'
        // ]);

        // if  ($request->file('image')){
        //     $validate['image'] = $request->file('image')->store('image-item');
        // }

        // Stok::create($validate);
        // return with('success', 'Succes store item');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreStokRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'nama' => 'required',
            'harga' => 'required',
            'image' => 'image|file|max:5000|',
            'total_stok' => 'required'
        ]);

        if  ($request->file('image')){
            $validate['image'] = $request->file('image')->store('image-item');
        }

        if  (Stok::create($validate)) return response()->json(['success', 'Succes store item']);

        return response()->json(['success', 'failed store item']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Stok  $stok
     * @return \Illuminate\Http\Response
     */
    public function show(Stok $stok)
    {
        return $stok;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Stok  $stok
     * @return \Illuminate\Http\Response
     */
    public function edit(Stok $stok)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateStokRequest  $request
     * @param  \App\Models\Stok  $stok
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stok $stok)
    {
        // dd($request->all());
        // return $request->all();

        $validate = $request->validate([
            'nama' => 'required',
            'harga' => 'required',
            'image' => 'image|file|max:5000',
            'total_stok' => 'required'
        ]);

        // $validate = Validator::make($request->all(),[
        //     'nama' => 'required|string',
        //     'harga' => 'required',
        //     'image' => 'image|file|max:5000',
        //     'total_stok' => 'required|string'
        // ]);

        // if ($validate->fails()) return response()->json(['error' => $validate->errors()], 401);

        // $stok = Stok::find($stok->id);

        if  ($request->file('image')) $request['image'] = $request->file('image')->store('image-item');

        if ($stok->update([
            'nama' => $request->nama,
            'harga' => $request->harga,
            'image' => $request->image,
            'total_stok' =>$request->total_stok
         ])){
            return response()->json(['message' => 'Success update']);
        }

        // if  ($request->nama) $stok['nama'] = $request->nama;

        // if  ($request->harga) $stok['harga'] = $request->harga;

        // if  ($request->total_stok) $stok['total_stok'] = $request->total_stok;

        // if  ($stok->save()) return response()->json(['message' => 'Success update']);

        return response()->json(['message' => 'Failed update']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Stok  $stok
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stok $stok)
    {
        if  (Stok::destroy('id', $stok->id)) return response()->json(['message' => 'Success delete']);
        return response()->json(['message' => 'Failed update']);
    }
}