<?php

namespace App\Http\Controllers;

use App\Models\Distributor;
use App\Http\Requests\UpdateDistributorRequest;
use Illuminate\Http\Request;

class DistributorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // dd($request->all());

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreDistributorRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'nama_distributor' => 'required',
            'nomor_distributor' => 'required',
            'wilayah_id' => 'required',
            'nik_distributor' => 'required|unique:distributors',
            'user_id' => 'required'
        ]);

        if  (Distributor::create($validateData)) return response()->json(['message' => 'Sukses sore']);

        return response()->json(['message' => 'Eror api'], 401);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Distributor  $distributor
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return Distributor::whereFirst('nama_distributor', $request->nama_distributor)->with('wilayah');
    }

    public function findWilayahByNamaDistributor(Request $request){
        $distributor = Distributor::whereFirst('nama_distributor', $request->nama_distributor);
        return $distributor->nama_wilayah;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Distributor  $distributor
     * @return \Illuminate\Http\Response
     */
    public function edit(Distributor $distributor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateDistributorRequest  $request
     * @param  \App\Models\Distributor  $distributor
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDistributorRequest $request, Distributor $distributor)
    {
        $data = [
            'nama_distributor' => 'required',
            'nomor_distributor' => 'required',
            'wilayah_id' => 'required',
            'user_id' => 'required'
        ];

        if ($request->nik_distributor != $distributor->nik_distributor){
            $data['nik_distributor'] = 'required|unique:distributors';
        }

        $validate = $request->validate($data);
        Distributor::where('id', $distributor->id)->update($validate);
        return with('success', 'Success update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Distributor  $distributor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Distributor $distributor)
    {
        Distributor::destroy('id', $distributor->id);
        return with('success', 'Success delete data');
    }
}