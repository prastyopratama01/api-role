<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Distributor>
 */
class DistributorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'nama_distributor' => $this->faker->unique()->name(),
            'nomor_distributor' => $this->faker->numerify('###-###-####'),
            'wilayah_id' => mt_rand(1, 3),
            'nik_distributor' => $this->faker->numerify('#######'),
            'user_id' => mt_rand(1, 10)
        ];
    }
}
