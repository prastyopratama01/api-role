<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Distributor;
use App\Models\Role;
use App\Models\Stok;
use App\Models\Wilayah;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();
        Distributor::factory(1)->create();

        Role::create([
            'name' => 'Super Admin'
        ]);

        Role::create([
            'name' => 'Admin'
        ]);

        Role::create([
            'name' => 'Distributor'
        ]);


        Wilayah::create([
            'nama_wilayah' => 'Sumsel'
        ]);

        Wilayah::create([
            'nama_wilayah' => 'Malang'
        ]);

        Wilayah::create([
            'nama_wilayah' => 'Surabaya'
        ]);

    }
}
