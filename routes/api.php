<?php

use App\Http\Controllers\DistributorController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\StokController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WilayahController;
use App\Http\Controllers\PemasukanController;
use App\Http\Controllers\PengeluaranController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function() {

    Route::post('/login', [UserController::class, 'authenticate']);

    Route::middleware('auth:sanctum')->group(function(){
        Route::post('/role-store', [RoleController::class, 'store']); // insert name role
        Route::get('/role-show-all', [RoleController::class, 'show']); // show all
        Route::get('/role-show-user', [RoleController::class, 'showUser']);
        Route::put('/role/{role}', [RoleController::class, 'update']);
        Route::delete('/role/{role}', [RoleController::class, 'destroy']);

        Route::post('/user-store', [UserController::class, 'store']); // insert acount
        Route::get('/user-show-all', [UserController::class, 'showAll']);
        Route::get('/user-role', [UserController::class, 'show']);
        Route::post('/user-update', [UserController::class, 'update']);
        Route::delete('/user/{user}', [UserController::class, 'destroy']);

        Route::post('/logout', [UserController::class, 'logout']);

        Route::post('/wilayah-store', [WilayahController::class, 'store']); // insert name wilayah
        Route::get('/wilayah-show-all', [WilayahController::class, 'show']); // show all
        Route::get('/wilayah-show-distributor', [WilayahController::class, 'showDistributor']); // show all
        Route::put('/wilayah/{nama_wilayah}', [WilayahController::class, 'update']);
        Route::delete('/wilayah/{nama_wilayah}', [WilayahController::class, 'destroy']);

        Route::post('/distributor-store', [DistributorController::class, 'store']); // insert name distributor
        Route::get('/distributor-show-all', [DistributorController::class, 'show']); // show all
        Route::get('/distributor-wilayah', [DistributorController::class, 'findWilayahByNamaDistributor']);
        Route::put('/distributor/{nama_distributor}', [DistributorController::class, 'update']);
        Route::delete('/distributor/{nama_distributor}', [DistributorController::class, 'destroy']);

        Route::resource('/stok', StokController::class);
        Route::resource('/pemasukan', PemasukanController::class);
        Route::resource('/pengeluaran', PengeluaranController::class);
    });
});